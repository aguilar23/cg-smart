<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CidadeGuia.com</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <style>
            html, body {
                background-color: #d47919;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .user-form{
                padding: 30px 15px;
                margin-bottom: 30px;
                background-color: rgba(0, 0, 0, 0.65);
                color :white;
                border-radius: 30px;
            }
            .user-form h3{
                text-align: center;
                font-weight: 700;
            }
            .form-check-input {
                display: none;
            }
            .header b{
                font-weight: 600;
            }
            .header{
                color: #fff;
                text-align: center;
                padding: 25px 0;
                font-weight: bold;
            }
            .user-form{
                padding: 30px 40px;
                margin-bottom: 30px;
                background-color: rgba(0, 0, 0, 0.65);
                color :white;
                border-radius: 30px;
            }
            .user-form h3{
                text-align: center;
                font-weight: 700;
            }
            .btn-group{
                margin-left:2px;
            }
            .btn-link{
                cursor: pointer;
            }
            label.btn{
                color: #fff;
                font-weight: bold;
            }
            .btn-link:hover{
                text-decoration: underline;
            }
            .form-page{
                display: none;
            }
            .form-page.active{
                display: block!important;
            }
            .btn-cta{
                font-weight: bold;
                padding: 15px 25px;
                font-size: 1.5rem;
            }
            .btn-check-all{
                font-weight: bold;
                color: white!important;
            }
            @media (max-width: 991px) {
                .btn-outline-light:hover,
                .btn-outline-light:active{
                    color: #f8f9fa;
                    background-color: transparent;
                    border-color: #f8f9fa;
                    box-shadow: none;
                }
                .header{
                    padding: 20px 0;
                    font-size: 38px;
                }
            }
        </style>
    </head>

    <body>
        <div class="container-fluid d-flex align-items-center justify-content-center" style="min-height: 100%;">
            <div class="row">
                <div class="col-12">
                    <form action="{{ url('/bem-vindo') }}/{{$token}}" method="POST" class="row d-flex justify-content-center">
                        @csrf
                        <div class="col-12 col-md-10">
                            <div class="row d-flex justify-content-around">
                                <div class="col-11 col-md-12 form-page active" id="page1">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">Queremos saber sobre o que você gosta, para oferecermos as melhores promoções!</h2>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta" onclick="displayPage(2);">Começar a economizar!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 col-md-12 form-page" id="page2">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">O que você gosta de comer?</h2>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <div class="row d-flex justify-content-center form-category" id="alimentacao">
                                                @foreach($categories as $key => $category)
                                                    @if ($category->macro_id == 1)
                                                        <div class="btn-group" data-toggle="buttons"><label class="btn btn-outline-light"><input class="form-check-input" type="checkbox" name="macro1[]" value="{{$category->id}}">{{$category->category_name}}</label></div>
                                                    @endif
                                                @endforeach
                                                <a id="checkAllAlimentacao" class="btn btn-link btn-check-all">Marcar todos</a>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta my-4" onclick="displayPage(3);">Quero mais descontos!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 col-md-12 form-page" id="page3">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">Quais serviços você costuma precisar?</h2>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <div class="row d-flex justify-content-center form-category" id="servicos">
                                                @foreach($categories as $key => $category)
                                                    @if ($category->macro_id == 2)
                                                        <div class="btn-group" data-toggle="buttons"><label class="btn btn-outline-light"><input class="form-check-input" type="checkbox" name="macro2[]" value="{{$category->id}}">{{$category->category_name}}</label></div>
                                                    @endif
                                                @endforeach
                                                <a id="checkAllServicos" class="btn btn-link btn-check-all">Marcar todos</a>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta my-4" onclick="displayPage(4);">Quero mais descontos!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 col-md-12 form-page" id="page4">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">Você frequenta quais tipos de loja?</h2>
                                        </div>
                                        <div class="row d-flex justify-content-center form-category" id="comercio">
                                            @foreach($categories as $key => $category)
                                                @if ($category->macro_id == 3)
                                                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-outline-light"><input class="form-check-input" type="checkbox" name="macro3[]" value="{{$category->id}}">{{$category->category_name}}</label></div>
                                                @endif
                                            @endforeach
                                            <a id="checkAllComercio" class="btn btn-link btn-check-all">Marcar todos</a>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta my-4" onclick="displayPage(5);">Quero mais descontos!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 col-md-12 form-page" id="page5">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">E para cuidar da beleza e da saúde?</h2>
                                        </div>
                                        <div class="row d-flex justify-content-center form-category" id="belezaSaude">
                                            @foreach($categories as $key => $category)
                                                @if ($category->macro_id == 5)
                                                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-outline-light"><input class="form-check-input" type="checkbox" name="macro4[]" value="{{$category->id}}">{{$category->category_name}}</label></div>
                                                @endif
                                            @endforeach
                                            <a id="checkAllBelezaSaude" class="btn btn-link btn-check-all">Marcar todos</a>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta my-4" onclick="displayPage(6);">Quero mais descontos!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 col-md-12 form-page" id="page6">
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="display-4 header">Para se divertir ou passear, o que te interessa?</h2>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center form-category" id="entretenimento">
                                            @foreach($categories as $key => $category)
                                                @if ($category->macro_id == 4)
                                                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-outline-light"><input class="form-check-input" type="checkbox" name="macro5[]" value="{{$category->id}}">{{$category->category_name}}</label></div>
                                                @endif
                                            @endforeach
                                            <a id="checkAllEntretenimento" class="btn btn-link btn-check-all">Marcar todos</a>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="btn btn-outline-light btn-cta my-4" onclick="submitRedirect();">Explorar os descontos!</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script
          src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script type="text/javascript">

        var curPage = 1;

        $( document ).ready(function(){
            // displayPage(curPage);
        });

        function displayPage(pageNum){
            $('#page' + curPage).fadeOut('fast', function(){
                $('#page' + pageNum).fadeIn('fast');
                $('.form-page.active').removeClass('active');
                $('#page' + pageNum).addClass('active');
                curPage = pageNum;
            });
        }

        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();
            // Smooth href # scroll
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 1300, 'swing');
        });

        $('#checkAllBelezaSaude').click(function(){
            if($(this).html() == 'Marcar todos'){
                $('#belezaSaude .btn:not(.active)').button('toggle');
                $(this).html('Desmarcar todos');
            } else {
                $(this).html('Marcar todos');
                $('#belezaSaude .btn.active').button('toggle');
            }
        });
        $('#checkAllEntretenimento').click(function(){
            if($(this).html() == 'Marcar todos'){
                $('#entretenimento .btn:not(.active)').button('toggle');
                $(this).html('Desmarcar todos');
            } else {
                $(this).html('Marcar todos');
                $('#entretenimento .btn.active').button('toggle');
            }
        });
        $('#checkAllComercio').click(function(){
            if($(this).html() == 'Marcar todos'){
                $('#comercio .btn:not(.active)').button('toggle');
                $(this).html('Desmarcar todos');
            } else {
                $(this).html('Marcar todos');
                $('#comercio .btn.active').button('toggle');
            }
        });
        $('#checkAllServicos').click(function(){
            if($(this).html() == 'Marcar todos'){
                $('#servicos .btn:not(.active)').button('toggle');
                $(this).html('Desmarcar todos');
            } else {
                $(this).html('Marcar todos');
                $('#servicos .btn.active').button('toggle');
            }
        });
        $('#checkAllAlimentacao').click(function(){
            if($(this).html() == 'Marcar todos'){
                $('#alimentacao .btn:not(.active)').button('toggle');
                $(this).html('Desmarcar todos');
            } else {
                $(this).html('Marcar todos');
                $('#alimentacao .btn.active').button('toggle');
            }
        });
        </script>
    </body>
</html>
