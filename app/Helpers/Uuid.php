<?php 

namespace App\Helpers;

class UUID {

    /**
     * @var
     */
    public $entropy;

    /**
     * @param string $prefix
     * @param bool $entropy
     */
    public function __construct($entropy = false)
    {
        $this->uuid = uniqid(rand(), $entropy);
    }

    /**
     * Limit the UUID by a number of characters
     * 
     * @param $length
     * @param int $start
     * @return $this
     */
    public function limit($length, $start = 0)
    {
        $this->uuid = substr($this->uuid, $start, $length);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return md5($this->uuid);
    }
} 
?>