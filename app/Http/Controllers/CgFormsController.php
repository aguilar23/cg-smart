<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Response;
use App\Cg_category;
use App\Question;

class CgFormsController extends Controller
{
    public function displayForm($token){
    	$responseModel = new Response();
        $response = $responseModel->where('token', $token)->first();
        if($response && $response->answered == 0){
	        $categories = Cg_category::all();
	    	\Log::info($categories);
	        $questions = Question::all();
	        return view('form', [
	            'categories' => $categories,
	            'questions' => $questions,
	            'token' => $token
	        ]);
        }else{
        	return "null";
        }
    }

    public function storeForm(Request $request, $token){

    }
}
