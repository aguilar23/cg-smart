<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;

use App\Helpers\UUID;
use App\Cg_user;
use App\Response;

class CgUsersController extends Controller
{
    public function index()
    {
    	$code = new UUID;
        return view('welcome', [
            'uuid' => $code
        ]);
    }

    public function store(Request $request){
    	try {
	        if($request->input('token') == 'f9a6db907e8c73c862defd3c372b9b21'){

		        $cg_user = new Cg_user();
		        $user = $cg_user->where('email', $request->input('email'))->first();
		        if($user){
			        $response = new Response();
			        $response->cg_user_id = $user->id;
			        $response->token = new UUID;
			        $response->answered = 0;
			        $response->save();
		        }else{
			        $cg_user->cg_id  = $request->input('id');
			        $cg_user->email  = $request->input('email');
			        $cg_user->fb  = $request->input('fb') ? $request->input('fb') : '' ;
			        $cg_user->save();

			        $response = new Response();
			        $response->cg_user_id = $cg_user->id;
			        $response->token = new UUID;
			        $response->answered = 0;
			        $response->save();
		        }
		        return url('bem-vindo/' . $response->token);
		        // return view('welcome', [
		        //     'uuid' => url('bem-vindo/' . $response->token)
		        // ]);
	        }else{
		        return view('welcome', [
		            'uuid' => ''
		        ]);
		    }
	    } catch(Exception $e) {
	    	\Log::info($e->getMessage());
	        // return view('welcome', [
	        //     'uuid' => $e->getMessage()
	        // ]);
	        return $e->getMessage();
	    }
    }
}