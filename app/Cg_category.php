<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cg_category extends Model
{
    protected $connection = 'mysql_cg';
    protected $table = 'category';
}
